package org.sample;

public class Polymorphism1 {
public void empDetails(String Name) {
  System.out.println("Employee name is ");
}
public void empDetails(int IDnum) {
	  System.out.println("Employee Id number is ");
}
public void empDetails(long salary) {
	  System.out.println("Employee salary is ");
}
public void empDetails(double percent) {
	  System.out.println("Employee percent is ");
}
public void empDetails(boolean Result) {
	  System.out.println("Result is ");
}
public static void main(String[] args) {
	Polymorphism1 g=new Polymorphism1();
	g.empDetails("AAA");
	g.empDetails(23425);
	g.empDetails(10000l);
	g.empDetails(98.5);
	g.empDetails(true);
}
}
