package org.sample;

public class StringConcept {
public static void main(String[] args) {
	String s1="Welcome";//String constant pool area
	char[] c= {'s','e','l','e','n','i','u','m'};
	String s2="Welcome";// String constant pool area
	String s3=new String("Welcome");//Stored in heap memory
	String s4=new String("Hello World");//Stored in heap memory
	String s5=new String(c);//Stored in heap memory
	System.out.println(s5);
	System.out.println(s1);
	System.out.println(s2);
	System.out.println(s3);
	System.out.println(s4);
	String s = s1.concat("Java");
	System.out.println(s);
	s1=s1.concat("Java");
	System.out.println(s1);
}
}
