package org.sample;

import java.util.ArrayList;

public class ArrayList1 {
public static void main(String[] args) {
	ArrayList<String> list=new ArrayList<>();
	System.out.println(list.size());
	list.add("a");
	list.add("b");
	list.add("c");
	System.out.println(list.size());
	list.add("d");
	System.out.println(list);
	System.out.println(list.size());
	list.set(0,"d");
	System.out.println(list);
	list.remove(0);
	System.out.println(list);
	list.remove("c");
	System.out.println(list);
	
}
}
