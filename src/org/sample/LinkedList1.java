package org.sample;

import java.util.LinkedList;

public class LinkedList1 {
public static void main(String[] args) {
	LinkedList<String> list=new LinkedList<>();
	list.add("a");
	list.add("b");
	list.add("c");
	list.add("d");
	System.out.println(list);
	list.add(1,"a1");
	System.out.println(list);
	list.addFirst("a0");
	list.addLast("z");
	System.out.println(list);
}
}
