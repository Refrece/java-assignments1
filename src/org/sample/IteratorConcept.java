package org.sample;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorConcept {
public static void main(String[] args) {
	ArrayList<String> s=new ArrayList<>();
	s.add("a");
	s.add("b");
	s.add("c");
	s.add("d");
	s.add("e");
	System.out.println(s);
	Iterator<String> itr=s.iterator();
	while (itr.hasNext()) {
		Object fetch_value=itr.next();
		System.out.println(fetch_value +" ");
		
		
	}
}
}
